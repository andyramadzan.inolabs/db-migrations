const express = require('express');
const bodyParser = require("body-parser");
const jsdom = require("jsdom");
const app = express();
const hostname = '127.0.0.1';
const port = 3000;
const mysql = require('mysql');

app.use(bodyParser.urlencoded({extended: false}));

const con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "nodokter"
});

const options = {
    runScripts: 'dangerously',
    resources: "usable",
    pretendToBeVisual: true,
    includeNodeLocations: true,
};

const {JSDOM} = jsdom;
const {window} = new JSDOM(`
<!DOCTYPE html>
<div id="editorjs"></div>
<script src="https://cdn.jsdelivr.net/npm/@editorjs/editorjs@2.20.0"></script>
<!--<script src="https://cdn.jsdelivr.net/npm/codex.editor.header@2.0.4/dist/bundle.js"></script>-->
<script src="https://cdn.jsdelivr.net/npm/@editorjs/table@latest"></script>
<script src="https://cdn.jsdelivr.net/npm/@editorjs/header@latest"></script>
<script src="https://cdn.jsdelivr.net/npm/@editorjs/link@latest"></script>
<script src="https://cdn.jsdelivr.net/npm/@editorjs/checklist@latest"></script>
<script src="https://cdn.jsdelivr.net/npm/@editorjs/embed@latest"></script>
<script src="https://cdn.jsdelivr.net/npm/@editorjs/quote@latest"></script>
<script src="https://cdn.jsdelivr.net/npm/@editorjs/inline-code@latest"></script>
<script src="https://cdn.jsdelivr.net/npm/@editorjs/warning@latest"></script>
<script src="https://cdn.jsdelivr.net/npm/@editorjs/code@latest"></script>
<script src="https://cdn.jsdelivr.net/npm/@editorjs/marker@latest"></script>
<script src="https://cdn.jsdelivr.net/npm/@editorjs/underline@latest"></script>
<script src="https://cdn.jsdelivr.net/npm/@editorjs/delimiter@latest"></script>
<script src="https://cdn.jsdelivr.net/npm/@editorjs/paragraph@latest"></script>
<script src="https://cdn.jsdelivr.net/npm/@editorjs/list@latest"></script>
<script src="https://cdn.jsdelivr.net/npm/@editorjs/raw@latest"></script>
<!--<script src="https://cdn.jsdelivr.net/npm/@editorjs/image@latest"></script>-->
<script src="https://cdn.jsdelivr.net/npm/@editorjs/simple-image@latest"></script>
<script>
    
    const editor = new EditorJS({
        holder: 'editorjs',
        tools: {
            header: Header,
            image: SimpleImage,
            checklist:Checklist,
            list: List,
            raw: RawTool,
            quote: Quote,
            Code: CodeTool,
            warning: Warning,
            Marker: Marker,
            delimiter:Delimiter,
            underline:Underline,
            paragraph:Paragraph,
            inlineCode:InlineCode,
            table: Table
        }
    })

    let me = this
    editor.isReady.then(()=>{
        me.editor = editor
    })

</script>`, options);

window.blocks = null

function convert() {
    con.connect(function(err) {
        if (err) throw err;
        con.query("SELECT news_id,news_content FROM site_news WHERE news_id < 10", function (err, result, fields) {
            if (err) throw err;
            Object.keys(result).forEach(function(key) {
                let row = result[key];
                let newsId = parseInt(row.news_id);
                // console.log(newsId)

                // ganti path gambar
                let oldPath = 'https://www.nodokter.com/wp-content/uploads/';
                let newPath = 'https://dev-assets.nodokter.com/wp-content/uploads/';
                let newsContentBlock = row.news_content.replace(oldPath,newPath)
                // yg di render harus format html
                window.editor.blocks.renderFromHTML(newsContentBlock);
                window.editor.save().then(data => {
                    con.query("UPDATE site_news SET news_content_block='"+JSON.stringify(data)+"'WHERE news_id='"+newsId+"'", function (err, result,) {
                        console.log("convert succes - id : "+newsId)
                    })
                })
              });
        })
    })
}



app.listen(port, hostname, function () {
    console.log('initilize editor js')
    // tunggu init editor js dulu
    setTimeout(() => {
        convert()
    }, 3000)
});
